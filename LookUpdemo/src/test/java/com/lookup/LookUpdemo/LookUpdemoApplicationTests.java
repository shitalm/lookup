package com.lookup.LookUpdemo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lookup.LookUpdemo.controller.LookupController;
import com.lookup.LookUpdemo.model.Airport;

import com.lookup.LookUpdemo.model.Location;
import com.lookup.LookUpdemo.service.LookupService;
import java.util.Arrays;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = LookupController.class)

public class LookUpdemoApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    LookupService lookupService;

    ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setup() throws Exception {
        Airport puneAirport = new Airport( "Pune", "VAPO", "Pune", "India", 18.582, 73.919,9.48);

        Airport mumbaiAirport = new Airport( "CHHATRAPATI SHIVAJI INTERNATIONAL", "VABB", "Pune", "India", 19.089, 72.868,121.88);

        when(lookupService.getNearestAirports(18.5204, 73.8567, 2)).thenReturn(Arrays.asList(puneAirport, mumbaiAirport));
    }

    @Test
    public void getNearestAirports() throws Exception {

        Location location = new Location(18.5204, 73.8567);
        mockMvc.perform(post("/v1/getAirports").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(location)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2))).andDo(print());
    }

    

    @Test
    public void getNearestAirportsWithoutLatlong() throws Exception {

        
        mockMvc.perform(post("/v1/getAirports").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(2)))
                .andExpect(jsonPath("$.errors[*]", containsInAnyOrder(
                        "latitude is required",
                        "longitude is required"
                )))
                .andDo(print());
    }

    @Test
    public void getNearestAirportsWithoutLatitude() throws Exception {

        Location location = new Location();
        location.setLongitude(73.8567);
        mockMvc.perform(post("/v1/getAirports").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(location)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[*]", containsInAnyOrder(
                        "latitude is required"
                )))
                .andDo(print());
    }

    @Test
    public void getNearestAirportsWithoutLongitude() throws Exception {

        Location location = new Location();
        location.setLatitude(73.8567);
        mockMvc.perform(post("/v1/getAirports").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(location)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[*]", containsInAnyOrder(
                        "longitude is required"
                )))
                .andDo(print());
    }

    @Test
    public void getNearestAirportsWithInvalidLongitude() throws Exception {

        Location location = new Location(18.5204, 181.90);
        mockMvc.perform(post("/v1/getAirports").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(location)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[*]", containsInAnyOrder(
                        "Longitude not valid"
                )))
                .andDo(print());
    }

    @Test
    public void getNearestAirportsWithInvalidLatitude() throws Exception {

        Location location = new Location(91.09, 73.8567);
        mockMvc.perform(post("/v1/getAirports").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(location)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[*]", containsInAnyOrder(
                        "Latitude not valid"
                )))
                .andDo(print());
    }

}
