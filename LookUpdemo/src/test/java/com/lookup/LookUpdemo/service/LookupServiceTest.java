/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lookup.LookUpdemo.service;

import com.lookup.LookUpdemo.dao.LookupDao;
import com.lookup.LookUpdemo.model.Airport;

import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author shitalm
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class LookupServiceTest {

    @Mock
    LookupDao lookupDao;

    @InjectMocks
    LookupServiceImpl lookupService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getNearestAirports() throws Exception {
        Airport airportDist = new Airport( "Pune", "Pune", "Pune", "India", 18.5204, 73.8567,2);

        Mockito.when(lookupDao.getNearestAirports(18.5204, 73.8567, 1)).thenReturn(Arrays.asList(airportDist));

        List<Airport> result = lookupService.getNearestAirports(18.5204, 73.8567, 1);
        assertEquals(1, result.size());
    }

}
