/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lookup.LookUpdemo.model;

/**
 *
 * @author shitalm
 */
public class Airport {

	String airportName;
	String airportCode;
	String city;
	String country;
	double latitude;
	double longitude;
	double distance;

	public Airport() {
	}

	public Airport(String airportName, String airportCode, String city, String country, double latitude,
			double longitude) {
		this.airportName = airportName;
		this.airportCode = airportCode;
		this.city = city;
		this.country = country;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getAirportName() {
		return airportName;
	}

	
	
	public Airport(String airportName, String airportCode, String city, String country, double latitude,
			double longitude, double distance) {
		super();
		this.airportName = airportName;
		this.airportCode = airportCode;
		this.city = city;
		this.country = country;
		this.latitude = latitude;
		this.longitude = longitude;
		this.distance = distance;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

}
