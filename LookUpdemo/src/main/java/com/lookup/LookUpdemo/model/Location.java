/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lookup.LookUpdemo.model;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Range;

/**
 *
 * @author shitalm
 */
public class Location {

    @NotNull(
            message = "latitude is required")
    @Range(min = -90, max = 90, message = "Latitude not valid")
    Double latitude;
    @NotNull(
            message = "longitude is required")
    
    @Range(min = -180, max = 180, message = "Longitude not valid")
    Double longitude;

    public Location() {
    }

    public Location(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}
