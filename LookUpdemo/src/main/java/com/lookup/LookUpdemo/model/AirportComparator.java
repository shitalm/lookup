/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lookup.LookUpdemo.model;

import java.util.Comparator;

/**
 *
 * @author shitalm
 */
public class AirportComparator implements Comparator<Airport> {

    @Override
    public int compare(Airport o1, Airport o2) {

        return (int )(o1.getDistance() - o2.getDistance());
    }

}
