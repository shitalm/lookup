/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lookup.LookUpdemo.utils;

import com.lookup.LookUpdemo.model.Airport;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import org.springframework.stereotype.Component;

/**
 *
 * @author shitalm
 */
@Component
public class AirportUtils {

    List<Airport> airports=new ArrayList<>();

    @Value("classpath:GlobalAirportDatabase.txt")
    private Resource res;

    @PostConstruct
    public void init() {

        
        try {
            List<String> lines = Files.readAllLines(Paths.get(res.getURI()),
                    StandardCharsets.UTF_8);

            for (String line : lines) {

                String[] words = line.split(":");

                Airport airport = new Airport();

                airport.setAirportCode(words[0]);
                airport.setAirportName(words[2]);
                airport.setCity(words[3]);
                airport.setCountry(words[4]);
                airport.setLatitude(Double.parseDouble(words[14]));
                airport.setLongitude(Double.parseDouble(words[15]));
                if (!airport.getAirportName().equals("N/A") && airport.getLatitude() != 0 && airport.getLongitude() != 0) {
                    airports.add(airport);
                }

            }
        } catch (Exception ex) {
            System.out.println("File not fount");
        }

    }

    public List<Airport> getAirports() throws IOException {

        return airports;
    }

    public double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        } else {
            double theta = lon1 - lon2;
            double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            if (unit.equals("K")) {
                dist = dist * 1.609344;
            } else if (unit.equals("N")) {
                dist = dist * 0.8684;
            }
            return (dist);
        }
    }
}
