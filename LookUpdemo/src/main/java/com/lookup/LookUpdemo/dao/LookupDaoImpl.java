/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lookup.LookUpdemo.dao;

import com.lookup.LookUpdemo.model.Airport;
import com.lookup.LookUpdemo.model.AirportComparator;
import com.lookup.LookUpdemo.utils.AirportUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author shitalm
 */
@Repository
public class LookupDaoImpl implements LookupDao {

    @Autowired
    AirportUtils airportUtils;

   

    @Override
    public List<Airport> getNearestAirports(double latitude, double longitude, int count) throws Exception{
        List<Airport> airports = airportUtils.getAirports();

        TreeSet<Airport> acedingairs = new TreeSet<>(new AirportComparator());

        for (Airport airport : airports) {
            double dist = airportUtils.distance(latitude, longitude, airport.getLatitude(), airport.getLongitude(), "K");
            Airport ad = airport;
            ad.setDistance(dist);
           
            acedingairs.add(ad);
        }
        
        
        List<Airport> topThree = new ArrayList<>();
        int i = 0;
        Iterator<Airport> iterator = acedingairs.iterator();
        while (i < count) {
            topThree.add(iterator.next());
            i++;
        }

        return topThree;

    }
    
    
    

}
