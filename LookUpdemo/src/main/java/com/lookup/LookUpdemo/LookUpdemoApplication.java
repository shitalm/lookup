package com.lookup.LookUpdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LookUpdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(LookUpdemoApplication.class, args);
    }

    
}
