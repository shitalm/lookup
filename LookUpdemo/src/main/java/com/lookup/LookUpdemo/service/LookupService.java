/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lookup.LookUpdemo.service;

import java.util.List;

import com.lookup.LookUpdemo.model.Airport;

/**
 *
 * @author shitalm
 */
public interface LookupService {

    List<Airport> getNearestAirports(double latitude, double longitude, int count) throws Exception;
}
