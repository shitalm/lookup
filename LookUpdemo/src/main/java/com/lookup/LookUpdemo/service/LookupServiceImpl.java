/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lookup.LookUpdemo.service;

import com.lookup.LookUpdemo.dao.LookupDao;
import com.lookup.LookUpdemo.model.Airport;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author shitalm
 */
@Service
public class LookupServiceImpl implements LookupService {

    @Autowired
    LookupDao lookupDao;

    @Override
    public List<Airport> getNearestAirports(double latitude, double longitude, int count) throws Exception {
        return lookupDao.getNearestAirports(latitude, longitude, count);
    }

}
