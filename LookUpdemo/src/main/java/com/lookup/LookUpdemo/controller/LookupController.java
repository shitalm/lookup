/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lookup.LookUpdemo.controller;


import com.lookup.LookUpdemo.model.Airport;
import com.lookup.LookUpdemo.model.Location;
import com.lookup.LookUpdemo.service.LookupService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shitalm
 */
@RestController
@RequestMapping(path = "/v1")
public class LookupController {

    @Autowired
    LookupService lookupService;
    
    @ApiOperation(value = "View a list of available nearest airports")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved airports")})
    @PostMapping(path = {"/getAirports", "/getAirports/{count}"})
    public List<Airport> getNeareastAirports(@Valid @RequestBody Location location, @PathVariable(required = false) Optional<Integer> count) throws IOException, Exception {
        int numberOfLocations = 2;

        if (count.isPresent()) {

            numberOfLocations = count.get();
            if (numberOfLocations > 10) {
                numberOfLocations=10;
            }
        }
        return lookupService.getNearestAirports(location.getLatitude(), location.getLongitude(), numberOfLocations);
    }

}
