package com.lookup.LookUpdemo;

import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2

public class SwaggerConfig {
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage("com.lookup.LookUpdemo.controller"))              
          .paths(PathSelectors.any())                          
          .build().apiInfo(metaData());                                           
    }
    
    private ApiInfo metaData() {
        List<VendorExtension> arrExtensions=new ArrayList<>();
        ApiInfo apiInfo = new ApiInfo("Lookup Rest Service", "Lookup Rest Service", "1.0", "", new springfox.documentation.service.Contact("Shital Medsinge", "", "smedsinge@gmail.com"), "", "",arrExtensions );
        return apiInfo;
    }
}
